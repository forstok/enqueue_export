# frozen_string_literal: true

require 'enqueue_export/version'

module EnqueueExport
  class Error < StandardError; end
  attr_accessor :pending_exports

  def self.perform(model, args: nil)
    # Request per Second
    rps = args&.delete(:rps)
    # Maximum number of queues per channel
    q_per_channel = args&.delete(:queues_per_channel) || {}

    # queue count obj is used for BOTH rps and queue per channel functions
    q_count = {}

    group_by(model, args).each do |data|
      batch_size(data).each do |batch|
        # Max queues per account
        channel_id = batch[0].channel_id
        account_id = batch[0].account_id
        max_queues = q_per_channel[channel_id]
        key = "#{channel_id}_#{account_id}"
        if max_queues
          if get_queue(key, q_count) >= max_queues
            next
          end
        end

        worker_id = generate_id
        ids = batch.map(&:id)
        public_send(model).where(id: ids).update_all(worker_id: worker_id)

        # RPS
        run_at =
          if rps&.dig(channel_id)
            # Formula: Ceiling of (current queues / channel RPS) becomes the number of seconds from now
            ((get_queue(key, q_count)/rps[channel_id].to_f).ceil).seconds.from_now
          end

        delayed_job(model, worker_id, run_at, channel_id)

        # Count queue if exists. If not, do nothing
        increment_queue(key, q_count)
      end
    end
  end

  def self.get_queue(key, q_count)
    q_count[key] ||= 0
  end

  def self.increment_queue(key, q_count)
    return unless q_count[key]
    q_count[key] += 1
  end

  def self.generate_id
    SecureRandom.hex(32)
  end

  def self.export(skip_account_ids: nil, skip_channel_ids: nil, only_channel_ids: nil, only_account_ids: nil)
    query = {
      local_feed_id: nil, worker_id: nil, response_id: nil, processed: false
    }

    query[:channel_id] = only_channel_ids if only_channel_ids
    query[:account_id] = only_account_ids if only_account_ids

    not_query = {}
    not_query.merge!(account_id: skip_account_ids) if skip_account_ids
    not_query.merge!(channel_id: skip_channel_ids) if skip_channel_ids

    PendingExport.where(query).where.not(not_query)
  end

  def self.response
    query =  { local_feed_id: nil, worker_id: nil, processed: false }
    PendingExport.where(query).where.not(response_id: nil)
  end

  def self.validity
    PendingValidity.where(
      worker_id: nil,
      validity: false,
      processed: false
    )
  end

  def self.qc
    PendingQc.where(worker_id: nil)
  end

  def self.group_by(model, args=nil)
    public_send(*[model, args].compact).group_by do |obj|
      [obj.mode, obj.account_id, obj.channel_id, obj.response_id]
    end.to_a
  end

  def self.batch_size(data)
    args = {
      channel_id: data[0][2],
      name: 'batch_size_mode'
    }
    size = Config.find_by(args).value[data[0][0]].to_i
    data[1].in_groups_of(size, false)
  end

  def self.delayed_job(model, worker_id, run_at = nil, channel_id = nil)
    service = if %w[export response].include? model
                Kernel.const_get("Worker::#{model.capitalize}Service")
              else
                Kernel.const_get("Check#{model.capitalize}Service")
              end

    priority = model == 'response' ? 0 : 3
    queue =
      case channel_id
      when 12
        'shopee'
      when 15
        'tokopedia'
      else
        'default'
      end

    Delayed::Job.enqueue(service.new(worker_id: worker_id), { queue: queue, priority: priority, run_at: run_at}.compact)
  end
end
