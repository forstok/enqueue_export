require 'securerandom'
class PendingExport;end
class Config;end
class Delayed
  class Job
    def self.enqueue;end
  end
end
module Worker
  class ExportService
    def initialize(worker_id:);end
  end

  class ResponseService
    def initialize(worker_id);end
  end
end
RSpec.describe EnqueueExport do
  subject { described_class.perform(mode) }
  let(:id) { 1 }
  let(:account_id) { 1 }
  let(:channel_id) { 1 }
  let(:listing_id) { 1 }
  let(:initial_attempt) { 0 }
  let(:mode) { 'export' }
  let(:worker_id) { nil }
  let(:local_feed_id) { nil }
  let(:available_methods) {
    {
      id: id,
      account_id: account_id,
      channel_id: channel_id,
      listing_id: 1,
      attempts: initial_attempt,
      mode: mode,
      worker_id: worker_id,
      local_feed_id: local_feed_id,
      error_message: nil
    }
  }
  let(:group) { [mode, account_id, channel_id] }
  let(:batch_size_result) { 2 }
  let(:pending_export) { double('Pending Exports Record', available_methods) }

  context 'When #perform is invoked' do
    let(:config_method) {
      {
        channel_id: 1,
        value: 2
      }
    }
    let(:config) { double('Config Record', config_method) }
    let(:delay_method) {
      {
        worker_id: nil
      }
    }
    let(:delay) { double(delay_method) }
    let(:pending_exports) { double('pending exports relation', { size: 1 }) }
    let(:group_by) { [[['testing', 1, 1], pending_exports]] }
    let(:random) { "bf7a65194ebb5257d1702a82969793429b41c9c7738b5fe23a39889c4e96b952" }
    let(:another_pending_export) { double('Pending Exports Record', available_methods.merge(id: 2)) }
    
    context 'When scheduler is export' do
      before do
        allow(SecureRandom).to receive(:hex).with(32).and_return(random)
        allow(PendingExport).to receive(:where).with(local_feed_id: nil, worker_id: nil).and_return(pending_exports)
        allow(pending_exports).to receive(:where).with(response_id: nil, processed: false).and_return(pending_exports)
        allow(Config).to receive(:find_by).and_return(config)
        allow(pending_exports).to receive(:public_send).with('export').and_return(group_by)
        allow(pending_exports).to receive(:group_by).and_return(group_by)
        allow(pending_exports).to receive(:in_groups_of).with(config.value, false).and_return([pending_exports])
        allow(pending_exports).to receive(:map).and_return([1,2])
        allow(pending_exports).to receive(:where).with(id: [1,2]).and_return(pending_exports)
        # allow(Delayed::Job).to receive(:enqueue)
      end

      it 'update worker_id enqueue delayed job' do
        group_by.size.times do
          pending_exports.size.times do
            expect(Delayed::Job).to receive(:enqueue)
            expect(pending_exports).to receive(:update_all)
          end
        end
        subject
      end
    end
    context 'When scheduler is response' do
      let(:mode) { 'response' }
      before do
        allow(SecureRandom).to receive(:hex).with(32).and_return(random)
        allow(PendingExport).to receive(:where).with(local_feed_id: nil, worker_id: nil).and_return(pending_exports)
        allow(pending_exports).to receive(:where).with(processed: true).and_return(pending_exports)
        allow(pending_exports).to receive(:where).and_return(pending_exports)
        allow(pending_exports).to receive(:not).with(response_id: nil).and_return(pending_exports)
        allow(Config).to receive(:find_by).and_return(config)
        allow(pending_exports).to receive(:public_send).with('response').and_return(group_by)
        allow(pending_exports).to receive(:group_by).and_return(group_by)
        allow(pending_exports).to receive(:in_groups_of).with(config.value, false).and_return([pending_exports])
        allow(pending_exports).to receive(:map).and_return([1,2])
        allow(pending_exports).to receive(:where).with(id: [1,2]).and_return(pending_exports)
      end

      it 'update worker_id enqueue delayed job' do
        group_by.size.times do
          pending_exports.size.times do
            expect(Delayed::Job).to receive(:enqueue)
            expect(pending_exports).to receive(:update_all)
          end
        end
        subject
      end
    end
  end
end
